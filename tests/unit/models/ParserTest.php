<?php
namespace tests\unit\models;

include ('/models/Parser.php');
use Codeception\Test\Unit;
use app\models\Parser;


class ParserTest extends Unit
{

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testParsingData()
    {
        $text =  'Sting';
        $expected  = 10;
        $actual = Parser::getParsingData($text);

        $this->assertCount($expected, $actual);

    }

    public function testTopTenVideoId()
    {
        $text =  'Sting';
        $expected  = 10;
        $actual = Parser::getTopTenVideoId($text);

        $this->assertCount($expected, $actual);

    }

    public function testPageParsing()
    {
        $text =  'u4vvl0S0k4k';
        $expected  = true;
        $actual = Parser::getPageParsing($text);

        $this->assertEquals($expected, isset($actual));

    }
}