<?php

return array(
    'index'     => 'request/index',
    'response'  => 'response/index',
    'info'      => 'ajaxResponse/info',
    'data'      => 'ajaxResponse/data'
);