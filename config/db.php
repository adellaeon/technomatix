<?php

    try
    {
        $dbObject = new PDO('sqlite:' . DB_NAME . '.db');
        // Set errormode to exceptions
        $dbObject->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        //$dbObject = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASS);
    }
    catch(PDOException $e)
    {
        echo $e->getMessage();
    }