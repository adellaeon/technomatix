<?php
namespace app\models;

use app\lib\Model;

/**
 * RequestModel Class
 *
 * @version 0.1.0
 */
class RequestModel extends Model
{

    /**
     * @var integer $id
     * @var string $request
     * @var string $routes
     */

    public $id;
    public $request;
    public $created_at;

    /**
     * Method `fieldsTable`
     *
     * @return array
     */
    public function fieldsTable()
    {
        return array(
            'id' => 'Id',
            'request' => 'Request',
            'created_at' => 'Created At'
        );
    }

}