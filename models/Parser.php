<?php
namespace app\models;

/**
 * Parser Class
 *
 * @version 0.1.0
 */

class Parser
{

    /**
     * Method `getParsingData`
     *
     * Returns parsed data for a new request
     *
     * @return $parsing
     */
    public static function getParsingData($string)
    {
        $string = urlencode(trim($string));

        $page = file_get_contents("http://www.youtube.com/results?search_type=videos&search_query=".$string);

        preg_match_all('/<a href=\"\/watch\?v=(.*?)\"/s', $page, $video_id, PREG_SET_ORDER);
        preg_match_all('/<h3 class=\"yt-lockup-title \">(.*?)<\/a>/s', $page, $title, PREG_SET_ORDER);
        preg_match_all('/<ul class=\"yt-lockup-meta-info\"><li>(.*?)<\/ul>/s', $page, $rating, PREG_SET_ORDER);
        preg_match_all('/<div class=\"yt-lockup-description yt-ui-ellipsis yt-ui-ellipsis-2\" dir=\"ltr\">(.*?)<\/div>/s', $page, $description, PREG_SET_ORDER);

        $parsing = self::getData($video_id, $title , $rating, $description);

        return $parsing;
    }


    /**
     * Method `getData`
     *
     * Collects parsed data
     *
     * @return array
     */
    public static function getData($video_id, $title, $rating, $description)
    {
        $parsing = array();

        foreach ($video_id as $i => $v)
        {
            $parsing[$i]['video_id'] =  $v[1];
        }

        foreach ($title as $i => $t)
        {
            $parsing[$i]['title'] = strip_tags($t[1]);
        }

        foreach ($rating as $i => $r)
        {
            $parsing[$i]['rating'] =  preg_replace('~\D~','', substr(strip_tags($r[1]), 3));
        }
        foreach ($description as $i => $d)
        {
            $parsing[$i]['description'] =  strip_tags($d[1]);
        }

        array_splice($parsing, 10);

        return $parsing;

    }


    /**
     * Method `getTopTenVideoId`
     *
     * parser gets the top 10 video id
     *
     * @return $results
     */
    public static function getTopTenVideoId($string)
    {
        $string = urlencode(trim($string));

        $page = file_get_contents("http://www.youtube.com/results?search_type=videos&search_query=" . $string);

        if (strpos($page, "/watch?v=") != FALSE)
        {
            preg_match_all("/href=\"\/watch\?v=([^\"]*)\"/sU", $page, $matches);
            $results = array_unique($matches[1]);
            array_splice($results,10);

            return $results;
        }
    }


    /**
     * Method `getPageParsing`
     *
     * parser page with 1 video
     *
     * @return $results
     */
    public function getPageParsing($id)
    {
        $id = trim($id);

        $page = file_get_contents("http://www.youtube.com/watch?v=".$id);

        preg_match_all('/<span id=\"eow-title\"(.*?)<\/span>/s', $page, $title, PREG_SET_ORDER);
        preg_match_all('/<div class=\"watch-view-count\">(.*?)<\/div>/s', $page, $rating, PREG_SET_ORDER);
        preg_match_all('/<p id=\"eow-description\" class=\"\" >(.*?)<\/p>/s', $page, $description, PREG_SET_ORDER);

        return array(
            'video_id' => $id,
            'title' => strip_tags($title[0][0]),
            'rating'=> preg_replace('~\D~','', strip_tags($rating[0][0])),
            'description' => strip_tags($description[0][0])
        );
    }

}