<?php

namespace app\models;

use app\lib\Model;

/**
 * ResponseModel Class
 *
 * @version 0.1.0
 */

class ResponseModel  extends Model
{
    /**
     * @var integer $id
     * @var integer $request_id
     * @var string $video_id
     * @var string $title
     * @var string $rating
     * @var string $description
     * @var string $created_at
     */

    public $id;
    public $request_id;
    public $video_id;
    public $title;
    public $rating;
    public $description;
    public $created_at;


    /**
     * Method `fieldsTable`
     *
     * @return array
     */
    public function fieldsTable(){
        return array(
            'id' => 'Id',
            'request_id' => 'Request Id',
            'video_id' => 'Video Id',
            'title' => 'Title',
            'rating' => 'Rating',
            'description' => 'Description',
            'created_at' => 'Created At'
        );
    }

    /**
     * Method `setData`
     *
     * set data in DB
     *
     * @return true
     */
    public static function setData($pars_data, $request_id)
    {
        if($pars_data)
        {
            foreach ($pars_data as $pars)
            {
                $model = new ResponseModel();
                $model->request_id = $request_id;
                $model->video_id = trim($pars['video_id']);
                $model->title = htmlentities(trim($pars['title']));
                $model->rating = (trim($pars['rating']));
                $model->description = htmlentities(trim($pars['description']));

                $model->save();
            }

            return true;
        }
    }

    /**
     * Method `checkData`
     *
     * check data from parsing and DB
     *
     * @return true
     */
    public static function checkData($results, $request_id)
    {
        $model = new ResponseModel();
        $data = array();
        $items = $model->getRowsByParams('request_id', $request_id);

        foreach ($items as $item)
        {
            $data[] = $item['video_id'];
        }

        $diff = array_diff($results, $data);

        //If there are new videos, run new parser
        if(count($diff))
        {
            foreach ($diff as $val)
            {
                //run  parser
                $pars_data = Parser::getPageParsing($val);
                $pars[0] = $pars_data ;
                //set data in DB
                ResponseModel::setData($pars, $request_id);
            }
        }
        return true;
    }

    /**
     * Method `averageRating`
     *
     * get average rating from $model
     *
     * @return $this
     */
    public static function averageRating($model)
    {
        if($model){
            $rating = 0;
            $count = count($model);

            foreach ($model as $item){
                $rating += $item['rating'];
            }

            return number_format($rating/$count, 1, '.', ' ');
        }
    }
}