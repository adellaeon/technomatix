<h1>Страница ответа</h1>

<div class="row">

    <div class="col-md-3">

        <h4>Cписок запросов для YouTube</h4>
<br />

        <table class="table" id="table_request">

            <?php if($request):
                foreach($request as $req):  ?>
                    <tr>
                        <td id="<?=$req['id']?>" >
                            <?=$req['request'];?>
                        </td>
                    </tr>
                <?php endforeach;
            endif; ?>

        </table>
    </div>

    <div class="col-md-3">

        <h4 class="title">Общая информация</h4>
        <br />


        <div id="info"></div>
    </div>

    <div class="col-md-6">
        <h4 class="title">Результаты по запросу</h4>
        <br />

        <div id="result"></div>
    </div>

</div>

<script src="/web/js/response.js"></script>