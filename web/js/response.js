function getInfo(id)
{
    $.ajax({
        async: true,
        type: "POST",
        url: 'info',
        error: function () {
            alert('Ошибка обработки данных');
        },
        success: function (data) {
            var result = $.parseJSON(data);
            if(!result.error ){
                $('#info').show();
                $('#info').html(result.info);
            }else{
                alert('Ошибка обработки данных');
            }
        },
        data: {
            id: id
        }
    });
}

function getResponse(id) {
    $.ajax({
        async: true,
        type: "POST",
        url: 'data',
        error: function () {
            alert('Ошибка обработки данных');
        },
        success: function (data) {
            var result = $.parseJSON(data);
            if(!result.error ){
                $('#result').show();
                $('#result').html(result.responseData);
            }else{
                alert('Ошибка обработки данных');
            }
        },
        data: {
            id: id
        }
    });
}


$(function()
{
    $('#info').hide();
    $('#result').hide();
    $('.title').hide();

    $("#table_request td").click(function(){

        $('.title').show();
        $('#table_request td').removeClass('bg-info');
        $('#'+$(this).attr('id')).addClass('bg-info');

        getInfo($(this).attr('id'));
        getResponse($(this).attr('id'));
    });

});