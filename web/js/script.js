$(function(){
    if(! $('.navbar-nav li').hasClass('active'))
    {
        var uri = location.href.split('/');
        if(uri.indexOf( 'response' ) != -1){
            $("#response").addClass("active");
        }else{
            $("#request").addClass("active");
        }
    }

    $("#request").click(function(){
        $("#request").addClass("active");
        $("#response").removeClass("active");
    });
    $("#response").click(function(){
        $("#response").addClass("active");
        $("#request").removeClass("active");
    });
});