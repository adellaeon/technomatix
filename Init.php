<?php

class Init
{
    private $init;

    public function __construct()
    {
        //создать бд и таблицы
        $this->init = include_once '/config/config.php';
        $this->DB();
        //запустить сервер
        $this->init = include_once '/config/init.php';
        exec('php -S '.$this->init['addr'] . ':' .$this->init['port']);

    }

    public function DB(){

        date_default_timezone_set('UTC');

        try {

            $dbObject = new PDO('sqlite:' . DB_NAME . '.db');
            $dbObject->setAttribute(PDO::ATTR_ERRMODE,
                PDO::ERRMODE_EXCEPTION);

            $dbObject->exec("CREATE TABLE IF NOT EXISTS request (
                    id INTEGER PRIMARY KEY, 
                    request VARCHAR(255), 
                    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP)");

            $dbObject->exec("CREATE TABLE IF NOT EXISTS response (
                    id INTEGER PRIMARY KEY, 
                    request_id INTEGER NOT NULL,
                    video_id VARCHAR(100), 
                    rating INTEGER,
                    title  VARCHAR(255) DEFAULT NULL,
                    description VARCHAR(255) DEFAULT NULL,
                    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP)");

            $dbObject = null;

        }
        catch(PDOException $e) {
            // Print PDOException message
            echo $e->getMessage();
        }
    }



}

new Init();