<?php

namespace app\controllers;

use app\lib\Controller;
use app\models\RequestModel;

/**
 * ResponseController Class
 *
 * @version 0.1.0
 */

class ResponseController extends Controller
{
    /**
     * Action `Index`
     *
     * @var string request
     *
     * @return $this
     */

    public function actionIndex()
    {
        $model = new RequestModel();
        $request = $model->getAllRows();
        $this->view->render('response', array('request' => $request));
    }

}