<?php
namespace app\controllers;

/**
 * RequestController Class
 *
 * @version 0.1.0
 */

use app\lib\Controller;
use app\models\RequestModel;
use app\models\ResponseModel;
use app\models\Parser;

class RequestController extends Controller
{

    /**
     * Action `Index`
     *
     * @var string $requests
     * @var string $search_data
     *
     * @return $this
     */

    public function actionIndex()
    {
        if( isset($_POST) && array_key_exists('formRequest', $_POST) && $_POST['formRequest'] != null )
        {
            //get a form request
            $requests = explode(',', $_POST['formRequest']);

            foreach( $requests as $request )
            {
                //check if there is an entry in the DB
                $search = new RequestModel(array('where' => 'request = "' . htmlspecialchars(trim($request)) . '"'));
                $search_data = $search->getOneRow();

                //if row is not in the DB
                if(!$search_data)
                {
                    $modelRequest = new RequestModel();
                    $modelRequest->request = htmlspecialchars(trim($request));

                    //save new request
                    if($modelRequest->save())
                    {
                        //run  parser
                        $pars_data = Parser::getParsingData($request);

                        //get  last row from RequestModel
                        $model = new RequestModel();
                        $last_request = $model->getLastRow();

                        //set parsed data in ResponseModel
                        ResponseModel::setData($pars_data,$last_request['id']);
                    }
                }
                else
                {

                    //start new parser, and pull first 10 videoId
                    $videos = Parser::getTopTenVideoId($request);

                    //check data from parsing and DB
                    ResponseModel::checkData($videos, $search_data['id']);

                }
            }
        }

        $this->view->render('request');
        return;

    }


}