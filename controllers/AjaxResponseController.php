<?php

namespace app\controllers;

use app\models\RequestModel;
use app\models\ResponseModel;


class AjaxResponseController
{

    public function actionInfo()
    {
        $error = false;
        $id = array_key_exists('id', $_POST) ? $_POST['id'] : '';

        if (!$id)
        {
            $error = true;
        }

        $modelResponse = new ResponseModel();
        $model = $modelResponse->getRowsByParams('request_id', $id);

        if (!$model)
        {
            $error = true;
        }

        $averageRating = ResponseModel::averageRating($model);

        $modelRequest = new RequestModel();
        $request = $modelRequest->getRowById($id);

        $info = '<dl>'
            . '<dt>Название запроса</dt>'
            . '<dd>' . html_entity_decode($request['request']) . '</dd>'
            . '<dt>Кол-во результатов в DB</dt>'
            . '<dd>' . number_format(count($model), 0, '.', ' ') . '</dd>'
            . ' <dt>Cредний рейтинг</dt>'
            . '<dd>' . $averageRating . '</dd>'
            . '</dl>';

        echo json_encode(array('info' => $info, 'error' => $error));
    }

    public function actionData()
    {
        $error = false;
        $id = array_key_exists('id', $_POST) ? $_POST['id'] : '';

        if (!$id)
        {
            $error = true;
        }

        $modelResponse = new ResponseModel();
        $model = $modelResponse->getRowsByParams('request_id', $id);

        if (!$model)
        {
            $error = true;
        }

        $responseData = '';

        foreach ($model as $item)
        {
            $responseData .= '<dl class="dl-horizontal">'
                . '<dt>Название</dt>'
                . '<dd>' . html_entity_decode($item['title']) . '</dd>'
                . '<dt>Рейтинг</dt>'
                . '<dd>' . number_format($item['rating'], 0, '.', ' ') . '</dd>'
                . ' <dt>Описание</dt>'
                . '<dd>' . html_entity_decode($item['description']) . '</dd>'
                . '<dt>Ссылка на видео</dt>'
                . '<dd><a target="_blank" href="http://www.youtube.com/watch?v=' . $item['video_id']. '">Перейти к видео</a></dd>'
                . '</dl><hr />';
        }

        echo json_encode(array('responseData' => $responseData, 'error' => $error));
    }

}